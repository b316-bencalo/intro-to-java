package com.zuitt;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "09212345006,09122345612", "Quezon City,Quezon Avenue");
        Contact contact2 = new Contact("John", "09212345006,09231223451", "Manila City,Cebu City");

        ArrayList<Contact> contactList = new ArrayList<>();
        contactList.add(contact1);
        contactList.add(contact2);

        phonebook.setContacts(contactList);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("********************************");
                System.out.println(contact.getName() + " has the following contact number:");
                for (String contactNumber : contact.getContactNumbers()) {
                    System.out.println(contactNumber);
                }
                System.out.println("*********************************");
                System.out.println(contact.getName() + " has been living in the following address:");
                for (String address : contact.getAddress()) {
                    System.out.println(address);
                }
                System.out.println("===========================================");
            }
        }

        User user1 = new User("Jose", 13, "jose@mail.com", "Manila City");

        System.out.println("Hi I'm " + user1.getName() + ". I'm " + user1.getAge() + " yrs old. You can reach me via my email address: " + user1.getEmail() + ". When I'm off work,I can be found at my house in " + user1.getAddress() + "." );

        Course course1 = new Course();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");

        Date startDate = new Date(123, 5 ,23);
        Date endDate = new Date(123, 7 ,23);

        course1.setName("Introduction to Java");
        course1.setDescription("An introduction to Java programming language. Good for beginners and career shifters");
        course1.setSeats(5);
        course1.setFee(1200.50);
        course1.setStartDate(startDate);
        course1.setEndDate(endDate);
        course1.setInstructor(user1);

        System.out.println("Welcome to the course " + course1.getName() + ". The description of this course is " + course1.getDescription() + ". There are only " + course1.getSeats() + " left. The fee for this course is " + course1.getFee() + " php. The course will start on " + dateFormat.format(course1.getStartDate()) + " and ends on " + dateFormat.format(course1.getEndDate()) + ".");
    }
}
