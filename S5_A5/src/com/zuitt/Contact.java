package com.zuitt;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact(){}

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // Getters
    public String getName() {
        return name;
    }

    public String[] getContactNumbers() {
        return contactNumber.split(",");
    }

    public String[] getAddress() {
        return address.split(",");
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
