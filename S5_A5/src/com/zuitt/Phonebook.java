package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts =  new ArrayList<Contact>();

    public Phonebook(){}

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
}
