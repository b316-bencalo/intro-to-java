package com.zuitt;

import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private User instructor;

    public Course(){}

    public Course(String name, String description, int seats, double fee, Date startDate, Date endDate, User instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getSeats() {
        return seats;
    }

    public double getFee() {
        return fee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public User getInstructor() {
        return instructor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }
}
