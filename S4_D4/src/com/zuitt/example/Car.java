package com.zuitt.example;

public class Car {
    // Properties/Attributes - the characteristics of the object the class will create
    // Constructor - method to create the object and instantiate with its initialized value
    // Getters and Setters - are methods to get values of an objects properties or set them
    // Methods - actions that an object can perform or do.

    // public access - the variable/property in the class is accessible anywhere in the application
    // private - limits the access and ability to get or set a variable/method to only within its own class.
    // getters - methods that return the value of the property
    // setters - methods that allow us to set the value of a property.

    private String make;
    private String brand;
    private int price;
    private Driver carDriver;

    // Constructor is a method which allows us to set the initial value of an instance

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    // Getters and Setters for our properties
    // Getters return a value so, therefore we must add the data type of the value returned

    // getters
    public String getMake() {
        return make;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    //setters
    public void setMake(String make) {
        this.make = make;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    /*
        Classes have relationship
        Composition allows modelling objects to be made up of other objects. Classes can have instances of other classes
    * */

    // Custom method to retrieve the car driver's name

    public String getCarDriverName() {
        return this.carDriver.getName();
    }

    public void start() {
        System.out.println("Vroom Vroom!");
    }
}
