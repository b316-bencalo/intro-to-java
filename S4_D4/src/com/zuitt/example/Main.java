package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
       /* Car car1 = new Car();
        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println(car1.price);

        car1.make = "Veyron";
        car1.brand = "Bugatti";
        car1.price = 200000;

        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println(car1.price);

        // Create two new instances of the car class and save it in a variable called car2 and car3 respectively

        *//*
            Access the properties of the instance and update its values.
            make = String
            brand = String
            price = int
            You can come up with your own values
             Print the values of each property of the instance.
        *//*

        Car car2 = new Car();

        car2.make = "Car2Make";
        car2.brand = "Car2Brand";
        car2.price = 200000;
        System.out.println(car2.brand);
        System.out.println(car2.make);
        System.out.println(car2.price);

        Car car3 = new Car();

        car3.make = "Car3Make";
        car3.brand = "Car3Brand";
        car3.price = 300000;
        System.out.println(car3.brand);
        System.out.println(car3.make);
        System.out.println(car3.price);*/

        Driver driver1 = new Driver("Alejandro", 25);
        System.out.println(driver1.getName());

        Car car1 = new Car("Veyron", "Innova", 123, driver1);
        car1.start();
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getBrand());
        System.out.println(car1.getMake());
        System.out.println(car1.getPrice());
        System.out.println(car1.getCarDriverName());

        /*
            Mini-Activity
            Create a new class called Animal with the following attributes
            name - string
            color - string

            add constructors, getters and setters for the class
        * */

        Animal animal1 = new Animal("Winnie", "White");
        System.out.println("Name: " + animal1.getName());
        System.out.println("Color: " + animal1.getColor());
        animal1.call();

        Dog dog1 = new Dog("Test", "Test", "Test");
        dog1.greet();
    }
}
