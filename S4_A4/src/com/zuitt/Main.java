package com.zuitt;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Johnny", 10, "johnny@mail.com", "Quezon City");

        Course course1 = new Course();

        Date startDate = new Date(123, 6, 13);
        Date endDate = new Date(123, 7, 13);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");

        course1.setName("Introduction to  Java");
        course1.setDescription("An introduction to Java for career shifters");
        course1.setSeats(5);
        course1.setFee(10000.50);
        course1.setStartDate(startDate);
        course1.setEndDate(endDate);
        course1.setInstructor(user1);

        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + " years old. You can reach me via my email: " + user1.getEmail() + ". When I'm off work, I can be found at my house in " + user1.getAddress() + ".");

        System.out.println("Welcome to the course " + course1.getName() + ". This course can be described as " + course1.getDescription() + ". Your instructor for this course is Mr. " + course1.getInstructor().getName() + ". Your start date will be on " + dateFormat.format(course1.getStartDate()) + " and ends on " + dateFormat.format(course1.getEndDate()) + ".");
    }
}
