package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        int average;

        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name: ");
        firstName = scanner.nextLine();

        System.out.println("Last Name: ");
        lastName = scanner.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade: ");
        secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade: ");
        thirdSubject = scanner.nextDouble();

        average = (int) ((firstSubject + secondSubject + thirdSubject) / 3);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average);
    }
}
