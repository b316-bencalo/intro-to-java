package com.zuitt.example;
import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner myObj2 = new Scanner(System.in);
        System.out.print("How old are you: ");

        double age = myObj2.nextDouble();
        System.out.println("This is a confirmation that you are " + age + " years old.");
    }
}
