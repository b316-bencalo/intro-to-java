package com.zuitt.example;

public class Person implements Actions{
    @Override
    public void run() {
        System.out.println("zzzzzz....");
    }

    @Override
    public void sleep() {
        System.out.println("The person is running.");
    }

    @Override
    public void eat() {
        System.out.println("Yummy!");
    }

    @Override
    public void walk() {
        System.out.println("walking");
    }
}
