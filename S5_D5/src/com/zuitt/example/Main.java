package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        Person person1 = new Person();

        person1.run();
        person1.sleep();
        person1.eat();
        person1.walk();

        StaticPoly staticPoly = new StaticPoly();

        System.out.println(staticPoly.addition(1,2));
        System.out.println(staticPoly.addition(1,2, 3));
        System.out.println(staticPoly.addition(1.5 ,22.2));

        Parent parent1 = new Parent("John", 35);
        parent1.speak();
        parent1.greet();
        parent1.greet("Apple", "night");

        Child newChild = new Child();
        newChild.speak();

    }
}
