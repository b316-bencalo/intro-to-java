package com.zuitt.example;

public interface Actions {
    public void sleep();

    public void run();

    public void walk();

    public void eat();
}