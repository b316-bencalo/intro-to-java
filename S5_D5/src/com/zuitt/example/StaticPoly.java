package com.zuitt.example;

public class StaticPoly {
    // Static polymorphism, this is the ability to have multiple methods of the same name but changes forms base on the number of arguments ot the types of arguments.
    public int addition(int a, int b) {
        return a + b;
    }

    public int addition(int a, int b, int c) {
        return a + b + c;
    }
    public double addition(double a, double b) {
        return a + b;
    }
}
