package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] prime_numbers = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is: " + prime_numbers[0]);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);

    }
}
